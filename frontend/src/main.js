import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(VueAxios, axios)


// CSS
import './css/cssist.css'

// JS
import '../bower_components/reset.css/reset.css'
import '../bower_components/cssist/cssist.js'

Vue.config.productionTip = false

Vue.prototype.$axios = axios

new Vue({
  render: h => h(App),
}).$mount('#app')
