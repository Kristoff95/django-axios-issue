Django==2.1.5
django-cors-headers==2.4.0
djangorestframework==3.9.0
pytz==2018.9
virtualenv==16.2.0
virtualenvwrapper-win==1.2.5
