import axios from "axios";

// You can use your own logic to set your local or production domain
const baseDomain = "http://127.0.0.1:8000";
// const baseDomain = "https://jsonplaceholder.typicode.com";

// The base URL is empty this time due we are using the jsonplaceholder API
const baseURL = `${baseDomain}`;
let tokenStr = '-ebvcnft@qv#=o$cn5!y8crp33af8e_0_mu#&bcin)f9-!_4!7'

export default axios.create({
  baseURL,
  headers: {
    withCredentials: true,
    "Authorization": `Bearer ${tokenStr}`, 
    "Access-Control-Allow-Origin": "*", 
    responseType: 'json',
    xsrfCookieName: 'XSRF-TOKEN',
    xsrfHeaderName: 'X-XSRF-TOKEN'
    // proxy: {
    //   host: '127.0.0.1',
    //   port: 8000
    // }
  },
  proxy: {
    host: '127.0.0.1',
    port: 8000,
    auth: {
      username: 'admin',
      password: 'password123'
    }
  },
});
